'use strict'
const mysql = require('mysql')
//local mysql db connection
const dbConn = mysql.createConnection({
  host: 'localhost',
  user: 'book',
  password: '12345',
  database: 'node_mysql_crud_db',
  port: 3306,
})
dbConn.connect(function (err) {
  if (err) throw err
  console.log('Database Connected!')
})
module.exports = dbConn
